## 开始维护

[http://demo.battcn.com](http://demo.battcn.com "演示地址")

鉴于 `dubbo` 已经开始维护了 , 本项目也即将开启维护之旅

坐等 `apache-dubbo` 与 `dubbo-spring-boot-starter（现在还是开发版）` 


## 技术栈

- **spring-boot**
- **dubbo**
- **mybatis**
- **shiro**
- **zookeeper**
- **swagger**

----------

**后续引入 redis、rabbitmq 等技术栈**



